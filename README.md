Various firmwares for armv4t development board.

A small clib and common drivers are included.

A. Dureghello (C) 2018  angelo at sysam.it

License
=======

ar-v4t GPL is available under [GPL License](https://www.apache.org/licenses/LICENSE-2.://www.gnu.org/licenses/gpl.html).
