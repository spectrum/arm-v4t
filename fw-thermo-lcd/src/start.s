/*
 * Sysam di Angelo Dureghello (C) 2017
 *
 * start.s part of thermo lcd
 *
 */

/* Stack Sizes */
.set  UND_STACK_SIZE, 0x00000010
.set  ABT_STACK_SIZE, 0x00000010
.set  FIQ_STACK_SIZE, 0x00000080
.set  IRQ_STACK_SIZE, 0x00000080
.set  SVC_STACK_SIZE, 0x00000080

/* Standard definitions of Mode bits and Interrupt (I & F)
 * flags in PSRs (program status registers)
 */
.set  ARM_MODE_USR, 0x10
.set  ARM_MODE_FIQ, 0x11
.set  ARM_MODE_IRQ, 0x12
.set  ARM_MODE_SVC, 0x13
.set  ARM_MODE_ABT, 0x17
.set  ARM_MODE_UND, 0x1B
.set  ARM_MODE_SYS, 0x1F
.set  I_BIT, 0x80
.set  F_BIT, 0x40

/* Addresses and offsets of AIC and PIO  */
.set  AT91C_BASE_AIC, 0xFFFFF000
.set  AT91C_AIC_FVR, 0xFFFFF104
.set  AIC_IVR, 256
.set  AIC_FVR, 260
.set  AIC_EOICR, 304

/* identify all GLOBAL symbols  */
.global _vec_reset
.global _vec_undef
.global _vec_swi
.global _vec_pabt
.global _vec_dabt
.global _vec_rsv
.global _vec_irq
.global _vec_fiq
.global _fault
.global _irq_handler
.global _fiq_handler

.text
.code 32

_vec_reset:		b	_init_reset
_vec_undef:		b	_fault
_vec_swi:		b	_vec_swi
_vec_pabt:		b	_fault
_vec_dabt:		b	_fault
_vec_rsv:		nop
_vec_irq:		b	_irq_handler
_vec_fiq:		b	_fiq_handler

_fiq_handler:
		/* adjust LR_irq */
		sub	lr, lr, #4
		/* clear the interrupt */
		ldr	r12, =AT91C_AIC_FVR
		ldr	r11, [r12]
		movs	pc, lr

.text
.align
_init_reset:
		ldr	r0, =_stack_end

		/* switch to Undefined Instruction Mode  */
		msr	CPSR_c, #ARM_MODE_UND|I_BIT|F_BIT
		mov	sp, r0
		sub	r0, r0, #UND_STACK_SIZE
		/* switch to Abort Mode */
		msr	CPSR_c, #ARM_MODE_ABT|I_BIT|F_BIT
		mov	sp, r0
		sub	r0, r0, #ABT_STACK_SIZE
		/* switch to FIQ Mode */
		msr	CPSR_c, #ARM_MODE_FIQ|I_BIT|F_BIT
		mov	sp, r0
		sub	r0, r0, #FIQ_STACK_SIZE
		/* switch to IRQ Mode */
		msr	CPSR_c, #ARM_MODE_IRQ|I_BIT|F_BIT
		mov	sp, r0
		sub	r0, r0, #IRQ_STACK_SIZE
		/* switch to Supervisor Mode */
		msr	CPSR_c, #ARM_MODE_SVC|I_BIT|F_BIT
		mov	sp, r0
		sub	r0, r0, #SVC_STACK_SIZE
		/* switch to System Mode */
		msr	CPSR_c, #ARM_MODE_SYS|I_BIT|F_BIT
		mov	sp, r0

		/* copy initialized variables .data section
		 * (Copy from ROM to RAM) */
		ldr	r1, =_etext
		ldr	r2, =_data
		ldr	r3, =_edata
1:		cmp     r2, r3
		ldrlo   r0, [r1], #4
		strlo   r0, [r2], #4
		blo     1b

		/* Clear uninitialized variables .bss section (Zero init)  */
		mov     r0, #0
		ldr	r1, =_bss_start
		ldr	r2, =_bss_end
2:		cmp     r1, r2
		strlo   r0, [r1], #4
		blo     2b

		/* Enter the c code  */
		b	main

_irq_handler:
		/*  Save interrupt context on the stack to allow nesting */
		sub	lr, lr, #4
		stmfd	sp!, {lr}
		mrs	lr, SPSR
		stmfd	sp!, {r0,r1,lr}

		/* Write in the IVR to support Protect Mode */
		ldr	lr, =AT91C_BASE_AIC
		ldr	r0, [r14, #AIC_IVR]
		str	lr, [r14, #AIC_IVR]

		/* Branch to interrupt handler in Supervisor mode */
		msr	CPSR_c, #ARM_MODE_SVC
		stmfd	sp!, {r1-r4, r12, lr}
		mov	lr, pc
		bx	r0
		ldmia	sp!, {r1-r4, r12, lr}
		msr	CPSR_c, #ARM_MODE_IRQ | I_BIT

		/* Acknowledge interrupt */
		ldr	lr, =AT91C_BASE_AIC
		str	lr, [r14, #AIC_EOICR]

		/* Restore interrupt context and branch back to calling code */
		ldmia	sp!, {r0,r1,lr}
		msr	SPSR_cxsf, lr
		ldmia	sp!, {pc}^

_fault:		b	_fault

.end
