/*
 * arm-v4t at91sam7s driver library
 *
 * init.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * This file is part of amtheatre firmware application.
 *
 * arm-v4t is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "i2c.h"
#include "dht11.h"
#include "display.h"
#include "reset.h"
#include "clock.h"
#include "time.h"
#include "console.h"
#include "interrupts.h"
#include "gpio.h"

#include <stdio.h>

#define DHT11_DATA_PA	30

/*
 * For this applciation, disaple all that's unused
 */
static void setup_for_minimal_consumption(void)
{
	int i;

	for (i = 0; i < 16; ++i) {
		if (i == 3 || i == 4)
			continue;
		gpio_configure(i, input);
		gpio_pull_up(i, 0);
	}

	for (i = 16; i < 32; ++i) {
		if (i == 30)
			continue;
		gpio_configure(i, input);
		gpio_pull_up(i, 0);
	}
}

void system_init(void)
{
	reset_button(1);
	reset_watchdog_disable();

	enable_main_clock();

	//init_interrupts();
	//init_sys_timer();

	init_gpio();

	//init_i2c();
	//init_display();
	//init_console();
	//init_dht11(DHT11_DATA_PA);

	//set_std_output(console_write);

	//setup_for_minimal_consumption();
}
