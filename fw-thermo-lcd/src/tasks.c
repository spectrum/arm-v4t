/*
 * thermo lcd
 *
 * tasks.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * thermo lcd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "display.h"
#include "console.h"
#include "time.h"
#include "dht11.h"
#include "clock.h"
#include "gpio.h"

#include <stdio.h>

extern uint32_t sys_ticks;
extern struct time tm;

static const int correction = 1;

static void uptime(void)
{
	console_set_cursor(0, 6);
	
	printf("%4dgg %02d:%02d:%02d", 
		tm.day,
		tm.hour,
		tm.min,
		tm.sec);
}

/*
 * Idle strategy
 *
 * If empty IO lines are set as inputs, pull up or down, there will be
 * a consumption, not ok.
 * If are set as outputs, driven low or high, that's much better.
 */
static void preidle(void)
{
	int i;

	for (i = 0; i < 32; ++i) {
		if (i == 3 || i == 4 || i == 30)
			continue;
		gpio_configure(i, output);
		gpio_set_value(i, 1);
	}

	disable_all_periph();
}

/*
 * Note: this fw works but mcu cannot reach an idle in the uA range,
 * since nunimum reached is around 10mA. M aybe, only entrring
 * slow clock mode can allow this.
 */
int run(void)
{
	struct th th;
	int i = 0;
	time_t time_time = 0;
	time_t time_read = 0;
	time_t now;

	console_clear();

	printf("T/H monitor\nv.091(alpha)\n\n");
	printf("T:        *C\n");
	printf("H:        %%\n");

	for (;;) {
		now = tm_sec();

		if ((now - time_time) >= 1) {
			time_time = now;
			uptime();
		}

		if ((now - time_read) > 10) {
			time_read = now;
			//init_dht11(30);
			if (dht11_read(&th) == 0) {
				console_set_cursor(3, 3);
				printf("       ");
				console_set_cursor(3, 3);
				th.ti -= correction;
				printf("%d.%d", th.ti, th.td);
				console_set_cursor(3, 4);
				printf("       ");
				console_set_cursor(3, 4);
				printf("%d.%d", th.hi, th.hd);
			} else {
				console_set_cursor(3, 3);
				printf("---.---");
				console_set_cursor(3, 4);
				printf("---.---");
			}
		}

		preidle();
		enter_idle();
		init_i2c();
	}

	return 0;
}
