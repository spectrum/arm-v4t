/*
 * wiegand - lcd
 *
 * main.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * thermo lcd is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "init.h"
#include "time.h"
#include "gpio.h"
#include "sam7s.h"

#include <stdio.h>

extern volatile uint32_t sys_ticks;

static volatile struct pio_regs *pio = (struct pio_regs *)SCM_PIOA;

#define bit(x)		(1 << x)

#define WIEGNAD_0	bit(10)
#define WIEGNAD_1	bit(9)

static void wiegand_init()
{
	enable_clock_gpio();

	gpio_periph_a_en(bit(9));
	gpio_periph_a_en(bit(10));

	pio->odr |= WIEGNAD_0;
	pio->odr |= WIEGNAD_1;
}

static int wiegand_read()
{
	uint8_t read = 0, state = 0;
	uint32_t ticks;
	uint32_t data = 0;

	ticks = sys_ticks;

	for (;;) {
		if (!(pio->pdsr & WIEGNAD_1)) {
			if (state == 0) {
				read = 1;
				ticks = sys_ticks;
				state = 1;
				data <<= 1;
				data |= 1;
			}
			continue;
		}
		if (!(pio->pdsr & WIEGNAD_0)) {
			if (state == 0) {
				read = 1;
				ticks = sys_ticks;
				state = 1;
				data <<= 1;
			}
			continue;
		}
		state = 0;

		/* timeout exit ? */
		if ((sys_ticks - ticks) > 500)
			break;
	}

	if (read) {
		data <<= 6;
		printf("read: %08x\n", (unsigned int)data);
	}

	return 0;
}

int main(int argc, char **argv)
{
	system_init();
	wiegand_init();

	for(;;)
		wiegand_read();

	return 0;
}

