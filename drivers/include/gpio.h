#ifndef __gpio_h
#define __gpio_h

#include <inttypes.h>

struct pio_regs {
	uint32_t per;
	uint32_t pdr;
	uint32_t psr;
	uint32_t res;
	uint32_t oer;
	uint32_t odr;
	uint32_t osr;
	uint32_t res2;
	uint32_t ifer;
	uint32_t ifdr;
	uint32_t ifsr;
	uint32_t res3;
	uint32_t sodr;
	uint32_t codr;
	uint32_t odsr;
	uint32_t pdsr;
	uint32_t ier;
	uint32_t idr;
	uint32_t imr;
	uint32_t isr;
	uint32_t mder;
	uint32_t mddr;
	uint32_t mdsr;
	uint32_t pudr;
	uint32_t puer;
	uint32_t pusr;
	uint32_t asr;
	uint32_t bsr;
	uint32_t absr;
};

enum gpio_type {
	input,
	output,
};

void init_gpio(void);

void gpio_multidrive(int bit, int state);
void gpio_configure(int bit, enum gpio_type type);
void gpio_pull_up(int bit, int state);
void gpio_set_value(int bit, int value);
int gpio_get_value(int bit);
void gpio_periph_a_en(int bit);

#endif /* __gpio_h */


