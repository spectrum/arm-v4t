#ifndef __time_h
#define __time_h

#include <inttypes.h>

struct time {
	uint8_t sec;
	uint8_t min;
	uint8_t hour;
	uint32_t day;
};

struct pit_regs {
	uint32_t mr;
	uint32_t sr;
	uint32_t pivr;
	uint32_t piir;
};

void init_sys_timer(void);

void delay_us(uint32_t us);
void delay_ms(uint32_t ms);

typedef uint32_t time_t;

time_t tm_sec(void);

#endif /* __time_h */
