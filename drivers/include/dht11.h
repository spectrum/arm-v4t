#ifndef __dht11_h
#define __dht11_h

struct th {
	int hi;
	int hd;
	int ti;
	int td;
};

void init_dht11(int gpio);
int dht11_read(struct th *th);

#endif /* __dht11_h */
