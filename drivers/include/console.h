#ifndef __console_h
#define __console_h

#include <stdint.h>

struct console_device {
	void (*clear)(void);
	void (*set_cursor)(int, int);
	void (*write)(char *);
};

void init_console(void);
void console_clear(void);
void console_register(struct console_device *console_dev);
void console_set_cursor(uint8_t col, uint8_t row);
void console_display_char_at_cur(char c);
void console_write(char *);

#endif /* __console_h */
