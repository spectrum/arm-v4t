#ifndef __clock_h
#define __clock_h

#include <inttypes.h>

#ifndef FREQ_CRYSTAL
#define FREQ_CRYSTAL 11059200
#endif
/*
 * Maximum allowed MCK (master clock) is 55 Mhz
 * but better don set 55 exactly, since approx. in calucation can
 * give final erroneous values.
 */
#ifndef FREQ_MCK
#define FREQ_MCK 50000000
#endif

/**
 * enables extrnal oscillator and internal main clock
 */
void enable_main_clock(void);

void enable_clock_gpio(void);
void enable_clock_i2c(void);
void enable_clock_pwmc(void);
void disable_all_periph(void);
void enter_idle(void);

#endif /* __clock_h */
