#ifndef __ep29_h
#define __ep29_h

struct signals {
	int clock;
	int miso;
	int mosi;
	int cs;
	int dc;
	int reset;
	int busy;
};

void ep29_init(struct signals *map_signals);
void ep29_clear(void);
void ep29_display_frame(void);
void ep29_set_image(const unsigned char* image_buffer);

#endif /* __ep29_h */
