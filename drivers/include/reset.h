#ifndef __reset_h
#define __reset_h

#include <stdint.h>

struct rstc_regs {
	uint32_t cr;
	uint32_t sr;
	uint32_t mr;
};

struct wdtc_regs {
	uint32_t cr;
	uint32_t mr;
	uint32_t sr;
};

#define MR_URSTEN	(1 << 0)
#define MR_KEY(x)	((x & 0xff) << 24)

#define MR_WDDIS	(1 << 15)

void reset_button(uint8_t on);
void reset_watchdog_disable(void);

#endif /* __reset_h */
