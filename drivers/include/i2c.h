#ifndef __i2c_h
#define __i2c_h

#include <inttypes.h>

struct i2c_regs {
	uint32_t cr;
	uint32_t mmr;
	uint32_t res;
	uint32_t iadr;
	uint32_t cwgr;
	uint32_t res2[3];
	uint32_t sr;
	uint32_t ier;
	uint32_t idr;
	uint32_t imr;
	uint32_t rhr;
	uint32_t thr;
};

void init_i2c(void);
void i2c_write(uint8_t addr, uint8_t *buff, uint8_t len);

#endif  /* __i2c_h */
