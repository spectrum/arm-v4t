#ifndef __interrupts_h
#define __interrupts_h

#include <stdint.h>

#define isr_entry() asm volatile \
	(" sub   lr, lr,#4\n" \
	 " stmfd sp!,{r0-r12,lr}\n" \
	 " mrs   r1, spsr\n" \
	 " stmfd sp!,{r1}")

#define isr_exit()  asm volatile \
	(" ldmfd sp!,{r1}\n" \
	 " msr   spsr_c,r1\n" \
	 " ldmfd sp!,{r0-r12,pc}^")


/* IRQ disable bit */
#define i_bit 0x80

/* FIQ disable bit */
#define f_bit 0x40

#define enable_interrupts() \
	asm("mrs  r0, cpsr\nand r0, %0\nmsr cpsr_c, r0\n" \
		::"i"(~(i_bit|f_bit)):"r0")

struct aic_regs {
	uint32_t smr[32];
	uint32_t svr[32];
	uint32_t ivr;
	uint32_t fvr;
	uint32_t isr;
	uint32_t ipr;
	uint32_t imr;
	uint32_t cisr;
	uint32_t res[2];
	uint32_t iecr;
	uint32_t idcr;
	uint32_t iccr;
	uint32_t iscr;
	uint32_t eoicr;
	uint32_t spu;
	uint32_t dcr;
};

void init_interrupts(void);
void interrupt_set_eoi(uint32_t val);
void interrupt_setup(int src, int prio, void (*handler)(void));
void interrupt_enable(int src);

#endif /* __interrupts_h */
