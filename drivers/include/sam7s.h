#ifndef __sam7s_h
#define __sam7s_h

enum periph {
	id_aic,
	id_sys,
	id_pioa,
	id_res,
	id_adc,
	id_spi,
	id_uart0,
	id_uart1,
	id_ssc,
	id_twi,
	id_pwmc,
	id_udp,
	id_tc0,
	id_tc1,
	id_tc2,
};

#define SCM_PIOA		0xfffff400
#define SCM_PMC			0xfffffc00

#define RSTC			0xfffffd00
#define WDTC			0xfffffd40

#define PWMC			0xfffcc000

#define TWI			0xfffb8000
#define PIT			0xfffffd30

#define AIC_BASE		0xfffff000

#endif /* __sam7s_h */
