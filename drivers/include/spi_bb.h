#ifndef __spi_bb_h
#define __spi_bb_h

#include <stdint.h>

void spi_bb_init(int b_clock, int b_miso, int b_mosi, int b_cs);
void spi_bb_xfer(uint8_t *out, uint8_t *in, int len);

#endif /* __spi_bb_h */
