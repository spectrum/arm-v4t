/*
 * arm-v4t at91sam7s driver library
 *
 * display.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "display.h"
#include "disp_oled.h"

static struct display_device *display_driver;

void init_display(void)
{
	init_disp_oled();
}

void display_register(struct display_device *drv)
{
	display_driver = drv;
}

struct display_device *display_get_current(void)
{
	return display_driver;
}

/* TO DO, register multiple devices */
