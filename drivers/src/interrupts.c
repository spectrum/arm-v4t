/*
 * arm-v4t at91sam7s driver library
 *
 * interrupts.c (SSD1306)
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sam7s.h"
#include "interrupts.h"

static volatile struct aic_regs *ic = (struct aic_regs *)AIC_BASE;

void interrupt_set_eoi(uint32_t val)
{
	ic->eoicr = val;
}

void interrupt_setup(int src, int prio, void (*handler)(void))
{
	/* Disable the interrupt first */
	ic->idcr = ~(1 << src);

	ic->smr[src] = prio;
	ic->svr[src] = (unsigned int)handler;

	/* Cear interrupt */
	ic->iccr = (1 << src);
}

void interrupt_enable(int src)
{
	ic->iecr = (1 << src);
}

void init_interrupts(void)
{
	enable_interrupts();

	ic->idcr = 0xffffffff;
}
