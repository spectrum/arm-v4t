/*
 * arm-v4t at91sam7s driver library
 *
 * clock.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sam7s.h"
#include "clock.h"

struct pmc_regs {
	uint32_t scer;		/* 0xFFFFFC00 */
	uint32_t scdr;
	uint32_t scsr;
	uint32_t res;
	uint32_t pcer;		/* 0xFFFFFC10 */
	uint32_t pcdr;
	uint32_t pcsr;
	uint32_t res2;
	uint32_t ckgr_mor;	/* 0xFFFFFC20 */
	uint32_t ckgr_mcfr;
	uint32_t res3;
	uint32_t ckgr_pllr;
	uint32_t mckr;		/* 0xFFFFFC30 */
	uint32_t res4[2];
	uint32_t pck0;
	uint32_t pck1;		/* 0xFFFFFC40 */
	uint32_t pck2;
	uint32_t res5[6];
	uint32_t ier;		/* 0xFFFFFC60 */
	uint32_t idr;
	uint32_t sr;		/* 0xFFFFFC68 */
	uint32_t imr;
};

#define PMC_SR_MOSCS		(1 << 0)
#define PMC_SR_LOCK		(1 << 2)
#define PMC_SR_MCKRDY		(1 << 3)

#define CKGR_PLLR_DIV(x)	((x & 0xff) << 0)
#define CKGR_PLLR_COUNT(x)	((x & 0x3f) << 8)
#define CKGR_PLLR_MUL(x)	(x << 16)
#define CKGR_PLLR_USBDEV(x)	((x & 0x03) << 28)

#define CKGR_MOR_MOSCEN		(1 << 0)
#define CKGR_MOR_OSCOUNT(x)	((x & 0xff) << 8)

#define PMC_MCKR_CSS(x)		((x & 0x03) << 0)
#define PMC_MCKR_PRES(x)	((x & 0x07) << 2)

/*
 * Crystal can be 3 to 20 Mhz
 * Master clock max can be max 55Mhz
 * Pll input min is 1Mhz
 *
 *                                         selector      prescaler
 *   crystal  moscen    mainck                     |\
 *                                     |mul|   ----| |
 * --|ext|---| MAINOSC |--> divider -->|PLL| ------| |---| PRESC |----MCK
 *   | in|                                     ----| |
 *                                                 |/
 */

#define PLLR_OUT_80_160		0
#define PLLR_OUT_150_180	2

#define PLL_RANGE		PLLR_OUT_80_160

#if (PLL_RANGE == PLLR_OUT_80_160)
#define PLL_LIMIT_HIGH		160
#define PLL_LIMIT_LOW		80
#else
#define PLL_LIMIT_HIGH		180
#define PLL_LIMIT_LOW		150
#endif

#define MOR_STARTUP_TIME	0x40
#define PLLR_SLOW_CYCLES	0x10

static const uint8_t presc[7] = {1, 2, 4, 8, 16, 32, 64};

static uint8_t calc_presc_divisor(uint8_t high, uint8_t low)
{
	uint8_t i, mid, delta, best_div = 1, last_delta = 0;
	uint16_t t, fmhz;

	fmhz = FREQ_MCK / 1000000;

	mid = ((high - low) >> 1) + low;

	for (i = 0; i < 7; i++) {
		t = fmhz * presc[i];
		if (t > high || t < low)
			continue;
		delta = (t > mid) ? t - mid : mid - t;

		if (!last_delta || delta < last_delta)
			best_div = i;

		last_delta = delta;
	}

	return best_div;
}

void enable_main_clock(void)
{
	volatile struct pmc_regs *pmc = (struct pmc_regs *)SCM_PMC;
	uint32_t pll;
	uint16_t div, mul;
	uint8_t prescaler;

	prescaler = calc_presc_divisor(PLL_LIMIT_HIGH, PLL_LIMIT_LOW);

	pll = FREQ_MCK * presc[prescaler];
	div = 1; /* bypass */

	mul = pll / FREQ_CRYSTAL;
	if (pll % FREQ_CRYSTAL > (FREQ_CRYSTAL / 2))
		mul++;
	mul--;

	/* enable main oscillator */
	pmc->ckgr_mor = CKGR_MOR_MOSCEN | CKGR_MOR_OSCOUNT(MOR_STARTUP_TIME);
	while(!(pmc->sr & PMC_SR_MOSCS))
		;

	/* setup pll, div 10, mul 50 */
	pmc->ckgr_pllr = CKGR_PLLR_COUNT(PLLR_SLOW_CYCLES) |
			CKGR_PLLR_USBDEV(1) |
			CKGR_PLLR_MUL(mul) |
			CKGR_PLLR_DIV(div);
	while(!(pmc->sr & PMC_SR_LOCK))
		;

	pmc->mckr = PMC_MCKR_PRES(prescaler);
	while(!(pmc->sr & PMC_SR_MCKRDY))
		;

	/* Select PLL */
	pmc->mckr |= PMC_MCKR_CSS(3);
	while(!(pmc->sr & PMC_SR_MCKRDY))
		;
}

void enable_clock_gpio(void)
{
	volatile struct pmc_regs *pmc = (struct pmc_regs *)SCM_PMC;

	pmc->pcer |= (1 << id_pioa);
}

void enable_clock_i2c(void)
{
	volatile struct pmc_regs *pmc = (struct pmc_regs *)SCM_PMC;

	pmc->pcer |= (1 << id_twi);
}

void enable_clock_pwmc(void)
{
	volatile struct pmc_regs *pmc = (struct pmc_regs *)SCM_PMC;

	pmc->pcer |= (1 << id_pwmc);
}

void disable_all_periph(void)
{
	volatile struct pmc_regs *pmc = (struct pmc_regs *)SCM_PMC;

	pmc->pcdr = 0x3000fef8;
}

void enter_idle(void)
{
	volatile struct pmc_regs *pmc = (struct pmc_regs *)SCM_PMC;

	/* set to idle */
	pmc->scdr |= 1;
}

