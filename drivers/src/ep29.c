/*
 * arm-v4t at91sam7s driver library
 *
 * ep29.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h>
#include "sam7s.h"
#include "spi_bb.h"
#include "gpio.h"
#include "io.h"
#include "time.h"
#include "ep29.h"

#define EPD_WIDTH		128
#define EPD_HEIGHT		296

#define FRAMEBUF_SIZE_1bpp	(EPD_WIDTH * EPD_HEIGHT / 8)
#define FRAMEBUF_SIZE		FRAMEBUF_SIZE_1bpp

// EPD2IN9 commands
#define DRIVER_OUTPUT_CONTROL			0x01
#define DATA_ENTRY_MODE_SETTING			0x11
#define BOOSTER_SOFT_START_CONTROL		0x0c
#define MASTER_ACTIVATION			0x20
#define DISPLAY_UPDATE_CONTROL_2		0x22
#define WRITE_RAM				0x24
#define WRITE_VCOM_REGISTER			0x2c
#define WRITE_LUT_REGISTER			0x32
#define SET_DUMMY_LINE_PERIOD			0x3a
#define SET_GATE_TIME				0x3b
#define SET_RAM_X_ADDRESS_START_END_POSITION	0x44
#define SET_RAM_Y_ADDRESS_START_END_POSITION	0x45
#define SET_RAM_X_ADDRESS_COUNTER		0x4e
#define SET_RAM_Y_ADDRESS_COUNTER		0x4f
#define TERMINATE_FRAME_READ_WRITE		0xff

static volatile struct pio_regs *pio = (struct pio_regs *)SCM_PIOA;
static struct signals s;
static int cursor_x, cursor_y;

const unsigned char lut_full_update[] =
{
	0x02, 0x02, 0x01, 0x11, 0x12, 0x12, 0x22, 0x22,
	0x66, 0x69, 0x69, 0x59, 0x58, 0x99, 0x99, 0x88,
	0x00, 0x00, 0x00, 0x00, 0xF8, 0xB4, 0x13, 0x51,
	0x35, 0x51, 0x51, 0x19, 0x01, 0x00
};

const unsigned char lut_partial_update[] =
{
	0x10, 0x18, 0x18, 0x08, 0x18, 0x18, 0x08, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x13, 0x14, 0x44, 0x12,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

static void ep29_reset(void)
{
	pio->sodr |= bit(s.reset);
	delay_ms(10);
	pio->codr |= bit(s.reset);
	delay_ms(200);
	pio->sodr |= bit(s.reset);
	delay_ms(200);
}

static void ep29_send_cmd(uint8_t cmd)
{
	uint8_t in;

	pio->codr |= bit(s.dc);
	spi_bb_xfer(&cmd, &in, 1);
}

static void ep29_send_data(uint8_t data)
{
	uint8_t in;

	pio->sodr |= bit(s.dc);
	spi_bb_xfer(&data, &in, 1);
}

static void ep29_wait_idle(void)
{
	while (pio->pdsr & bit(s.busy))
		delay_ms(100);
}

static void ep29_set_lut(const unsigned char *lut)
{
	int i;

	ep29_send_cmd(WRITE_LUT_REGISTER);

	for(i = 0; i < 30; i++) {
		ep29_send_data(lut[i]);
	}
}

static void ep29_set_memory_area(int x_start, int y_start,
				    int x_end, int y_end)
{
	ep29_send_cmd(SET_RAM_X_ADDRESS_START_END_POSITION);
	/* x point must be the multiple of 8 or last 3 bits will be ignored */
	ep29_send_data((x_start >> 3) & 0xff);
	ep29_send_data((x_end >> 3) & 0xff);
	ep29_send_cmd(SET_RAM_Y_ADDRESS_START_END_POSITION);
	ep29_send_data(y_start & 0xff);
	ep29_send_data((y_start >> 8) & 0xff);
	ep29_send_data(y_end & 0xff);
	ep29_send_data((y_end >> 8) & 0xff);
}

static void ep29_set_memory_ptr(int x, int y)
{
	ep29_send_cmd(SET_RAM_X_ADDRESS_COUNTER);
	/* x point must be the multiple of 8 or last 3 bits will be ignored */
	ep29_send_data((x >> 3) & 0xFF);
	ep29_send_cmd(SET_RAM_Y_ADDRESS_COUNTER);
	ep29_send_data(y & 0xFF);
	ep29_send_data((y >> 8) & 0xFF);
	ep29_wait_idle();
}

void ep29_update(void)
{
	int i;

	ep29_send_cmd(0x24);

	for (i = 0; i < 3096; i++)
	//_send_data(_screen[i]);
	//_send_data(buf[i]);
	delay_ms(1);
	//read_busy();
}

void ep29_init(struct signals *map_signals)
{
	s = *map_signals;

	spi_bb_init(s.clock, s.miso, s.mosi, s.cs);

	gpio_periph_a_en(bit(s.dc));
	gpio_periph_a_en(bit(s.reset));
	gpio_periph_a_en(bit(s.busy));

	/* Outputs */
	pio->oer |= bit(s.dc);
	pio->oer |= bit(s.reset);
	/* Inputs */
	pio->odr |= bit(s.busy);

	ep29_reset();

	ep29_send_cmd(DRIVER_OUTPUT_CONTROL);
	ep29_send_data((EPD_HEIGHT - 1) & 0xFF);
	ep29_send_data(((EPD_HEIGHT - 1) >> 8) & 0xFF);
	ep29_send_data(0x00);
	ep29_send_cmd(BOOSTER_SOFT_START_CONTROL);
	ep29_send_data(0xd7);
	ep29_send_data(0xd6);
	ep29_send_data(0x9d);
	ep29_send_cmd(WRITE_VCOM_REGISTER);
	ep29_send_data(0xa8);                     // VCOM 7C
	ep29_send_cmd(SET_DUMMY_LINE_PERIOD);
	ep29_send_data(0x1a);                     // 4 dummy lines per gate
	ep29_send_cmd(SET_GATE_TIME);
	ep29_send_data(0x08);                     // 2us per line
	ep29_send_cmd(DATA_ENTRY_MODE_SETTING);
	ep29_send_data(0x03);                     // X increment; Y increment

	ep29_set_lut(lut_full_update);
}

void ep29_clear(void)
{
	int i;

	ep29_set_memory_area(0, 0, EPD_WIDTH - 1, EPD_HEIGHT - 1);
	ep29_set_memory_ptr(0, 0);
	ep29_send_cmd(WRITE_RAM);

	/* send the color data */
	for (i = 0; i < EPD_WIDTH / 8 * EPD_HEIGHT; i++)
		ep29_send_data(0xff);

	cursor_x = 0;
	cursor_y = 0;
}

void ep29_set_image(const unsigned char* image_buffer)
{
	int i;

	ep29_set_memory_area(0, 0, EPD_WIDTH - 1, EPD_HEIGHT - 1);
	ep29_set_memory_ptr(0, 0);
	ep29_send_cmd(WRITE_RAM);
	/* send the image data */
	for (i = 0; i < EPD_WIDTH / 8 * EPD_HEIGHT; i++)
		ep29_send_data(image_buffer[i]);
}

void ep29_display_frame(void)
{
	ep29_send_cmd(DISPLAY_UPDATE_CONTROL_2);
	ep29_send_data(0xc4);
	ep29_send_cmd(MASTER_ACTIVATION);
	ep29_send_cmd(TERMINATE_FRAME_READ_WRITE);
	ep29_wait_idle();
}
