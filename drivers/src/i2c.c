/*
 * arm-v4t at91sam7s driver library
 *
 * i2c.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "clock.h"
#include "gpio.h"
#include "sam7s.h"
#include "i2c.h"
#include "time.h"

#define TWI_CR_START	(1 << 0)
#define TWI_CR_STOP	(1 << 1)
#define TWI_CR_MST_EN	(1 << 2)
#define TWI_CR_MST_DIS	(1 << 3)
#define TWI_CR_SWRST	(1 << 7)

#define TWI_SR_TXCOMP	(1 << 0)
#define TWI_SR_TXRDY	(1 << 2)
#define TWI_SR_NACK	(1 << 8)

#define TWI_MMR_MREAD	(1 << 12)

static volatile struct i2c_regs *twi = (struct i2c_regs *)TWI;
static uint32_t wait_nack_us;

static void i2c_set_speed(uint32_t speed_khz)
{
	uint32_t hz, ckdiv, div;
	uint8_t i;
	float chdiv, cldiv;

	wait_nack_us = (float)(1.0f / speed_khz * 2 * 1000);

	ckdiv = 1;
	div = 1;

	for (i = 0; i < ckdiv; ++i)
		div *= 2;

	/*
	 * cldiv or chdiv:
	 * cldiv = ((Tl / Tmck) - 3) / 2
	 */
	hz = speed_khz * 1000 * 2;

	cldiv = chdiv = ((1/(float)hz) / (1/(float)FREQ_MCK) - 3) / div;

	twi->cwgr = (ckdiv << 16) | ((uint8_t)chdiv << 8) | (uint8_t)cldiv;
}

static inline void i2c_wait_transfer_completed(void)
{
	while(!(twi->sr & TWI_SR_TXCOMP))
		;
}

void init_i2c(void)
{
	enable_clock_i2c();

	gpio_periph_a_en(3);
	gpio_periph_a_en(4);

	gpio_multidrive(3, 1);

	/* Disable interrupts */
	twi->idr = 0xffffffff;

	twi->cr = TWI_CR_SWRST;
	twi->cr = TWI_CR_MST_EN;

	i2c_set_speed(400);
}

static void wait_for_nack(void)
{
	delay_us(wait_nack_us);
}

void i2c_write(uint8_t addr, uint8_t *buff, uint8_t len)
{
	twi->cr = TWI_CR_MST_EN;

	twi->mmr |= (addr << 16);
	twi->mmr &= ~TWI_MMR_MREAD;

	while (len--) {
		twi->cr = TWI_CR_MST_EN | TWI_CR_START;

		twi->thr = *buff++;

		while (!(twi->sr & TWI_SR_TXRDY))
			;

		twi->cr = TWI_CR_STOP;

		wait_for_nack();
		if (twi->sr & TWI_SR_NACK)
			return;
	}

	i2c_wait_transfer_completed();
}

