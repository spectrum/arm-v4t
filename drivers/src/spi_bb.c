/*
 * arm-v4t at91sam7s driver library
 *
 * spi_bb.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdint.h>
#include "sam7s.h"
#include "clock.h"
#include "io.h"
#include "gpio.h"

static volatile struct pio_regs *pio = (struct pio_regs *)SCM_PIOA;

/*
 * bitbanging bits
 */
static int clock;
static int miso;
static int mosi;
static int cs;

void spi_bb_init(int b_clock, int b_miso, int b_mosi, int b_cs)
{
	enable_clock_gpio();

	clock = b_clock;
	miso = b_miso;
	mosi = b_mosi;
	cs = b_cs;

	gpio_periph_a_en(bit(clock));
	gpio_periph_a_en(bit(miso));
	gpio_periph_a_en(bit(mosi));
	gpio_periph_a_en(bit(cs));

	/* Outputs */
	pio->oer |= (1 << clock);
	pio->oer |= (1 << mosi);
	pio->oer |= (1 << cs);

	/* Inputs */
	pio->odr |= (1 << miso);

	pio->sodr |= bit(cs);
	pio->codr |= bit(clock);
}

static void spi_bb_byte_xfer(uint8_t out, uint8_t *in)
{
	int i, q = 0;

	for (i = 0; i < 8; ++i) {
		/* mode0, present data, MSB first */
		out & 0x80 ? (pio->sodr |= bit(mosi)) :
			     (pio->codr |= bit(mosi));
		out <<= 1;
		pio->sodr |= bit(clock);
		/* get the in value now, both just to balance out part */
		q <<= 1;
		if (pio->pdsr & bit(miso))
			q |= 1;
		pio->codr |= bit(clock);
	}
	/* reset MOSI line */
	pio->codr |= bit(mosi);
	*in = q;
}

void spi_bb_xfer(uint8_t *out, uint8_t *in, int len)
{
	pio->codr |= bit(cs);

	while (len--)
		spi_bb_byte_xfer(*out++, in++);

	pio->sodr |= bit(cs);
}
