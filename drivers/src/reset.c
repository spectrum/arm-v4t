/*
 * armv4t library
 *
 * reset.c
 *
 * This file is part of amtheatre firmware application.
 *
 * armv4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sam7s.h"
#include "reset.h"

void reset_watchdog_disable(void)
{
	volatile struct wdtc_regs *wdt = (struct wdtc_regs *)WDTC;

	wdt->mr = MR_KEY(0xa5) | MR_WDDIS;
}

void reset_button(uint8_t on)
{
	volatile struct rstc_regs *rst = (struct rstc_regs *)RSTC;

	if (on)
		rst->mr = MR_KEY(0xa5) | MR_URSTEN;
	else
		rst->mr = MR_KEY(0xa5) & ~MR_URSTEN;

}
