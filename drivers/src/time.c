/*
 * arm-v4t at91sam7s driver library
 *
 * time.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * arm-v4t is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "clock.h"
#include "time.h"
#include "sam7s.h"
#include "interrupts.h"

#define PIT_PITEN		(1 << 24)
#define PIT_PITIEN		(1 << 25)
#define PIT_PITS		(1 << 0)

#ifdef SYSTICK_1ms
#define SYS_FACT 	1
#else
/* Setting for 10 ms */
#define SYS_FACT 	10
#endif

#define COUNT_50MHZ	(3125 * SYS_FACT)

static volatile struct pit_regs *pit = (struct pit_regs *)PIT;

/*
 * System ticks, granularity: 1 tick is 1 msec
 */
uint32_t sys_ticks = 0;

/*
 * For time in seconds
 */
time_t sys_secs = 0;

struct time tm;

void timer_handler_system(void)
{
	if (pit->sr & PIT_PITS) {

		sys_ticks++;
		
		if (!(sys_ticks % (1000 / SYS_FACT))) {
			tm.sec++;
			sys_secs++;
			if (tm.sec == 60) {
				tm.sec = 0;
				tm.min++;
				if (tm.min == 60) {
					tm.min = 0;
					tm.hour++;
					if (tm.hour == 24) {
						tm.hour = 0;
						tm.day++;
					}
				}
			}
		}
		
		/* ack + end of interrupt */
		interrupt_set_eoi(pit->pivr);
	} else
		interrupt_set_eoi(0);
}

time_t tm_sec(void)
{
	/* uint32_t keeps 136 years */
	return sys_secs;
}

void delay_us(uint32_t us)
{
	uint32_t reg, i;

	while (us--) {
#if (FREQ_MCK == 50000000)
		for (i = 0; i < 2; ++i) {
			reg = pit->piir;
			while (pit->piir == reg)
				;
		}
		/* tuning */
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
		asm volatile("nop");
#endif
	}
}


void delay_ms(uint32_t ms)
{
	uint32_t reg;

	while (ms--) {
		reg = pit->piir;
		while (pit->piir == reg)
			;
		while (pit->piir != reg)
			;
	}
}

void init_sys_timer(void)
{
	uint32_t counter = 0;

	interrupt_setup(id_sys, 7, &timer_handler_system);
	interrupt_enable(id_sys);

	/*
	 * Counters increase at Mclk / 16
	 *
	 *i.e. for 50Mhz, 1 tick = 0,32us
	 */
#if (FREQ_MCK == 50000000)
	counter = COUNT_50MHZ;
#endif
	pit->mr = PIT_PITEN | PIT_PITIEN | (counter & 0xfffff);
}


