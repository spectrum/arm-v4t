/*
 * arm-v4t at91sam7s driver library
 *
 * console_128x64.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "font_8x8.h"
#include "console.h"
#include "console_128x64.h"
#include "disp_oled.h"
#include "display.h"

#include "string.h"

#define CONSOLE_SSD1306

#define COLS	(128 / 8)

static struct font_desc *font_desc = 0;
static int c_col = 0, c_row = 0;

static struct display_device *disp_drv;

void console_128x64_set_cursor(int col, int row)
{
	c_col = col;
	c_row = row;
}

void console_128x64_write(char *text)
{
	const unsigned char *font_char;
	char c;
	int i, len;

	len = strlen(text);

	for (i = 0; i < len; ++i) {

		c = *text++;

		if (c == '\n') {
			c_col = 0;
			c_row++;

			continue;
		}

		font_char = &font_desc->data[8 * c];
		disp_drv->write_at(font_char, c_col++, c_row);

		if (c_col == COLS) {
			c_col = 0;
			c_row++;
		}
	}
	disp_drv->update();
}

void console_128x64_clear(void)
{
	disp_drv->clear();
}

struct console_device cdev = {
	.set_cursor = console_128x64_set_cursor,
	.write = console_128x64_write,
	.clear = console_128x64_clear,
};

void console_128x64_init(void)
{
	disp_drv = display_get_current();

	/*
	 * actually, using a 8x8 font, to keep things simple
	 */
	font_desc = &font_8x8;

	disp_drv->set_font_dim(font_desc->width, font_desc->height);

	console_register(&cdev);
}
