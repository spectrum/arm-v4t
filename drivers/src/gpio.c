/*
 * arm-v4t at91sam7s driver library
 *
 * gpio.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * arm-v4t library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with arm-v4t.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sam7s.h"
#include "gpio.h"
#include "clock.h"

static volatile struct pio_regs *pio = (struct pio_regs *)SCM_PIOA;

void init_gpio(void)
{
	enable_clock_gpio();
}

void gpio_multidrive(int bit, int state)
{
	if (state)
		pio->mder |= (1 << bit);
	else
		pio->mddr |= (1 << bit);
}

void gpio_periph_a_en(int bit)
{
	pio->asr |= (1 << bit);
	pio->bsr &= ~(1 << bit);

	/* set into perih mode */
	pio->pdr |= (1 << bit);
}

void gpio_configure(int bit, enum gpio_type type)
{
	/* set into gpio mode */
	pio->per |= (1 << bit);

	if (type == output)
		pio->oer |= (1 << bit);
	else
		pio->odr |= (1 << bit);
}

void gpio_pull_up(int bit, int state)
{
	if (state) {
		pio->pudr &= ~(1 << bit);
		pio->puer |= (1 << bit);
	} else {
		pio->pudr |= (1 << bit);
		pio->puer &= ~(1 << bit);
	}
}

void gpio_set_value(int bit, int value)
{
	if (value)
		pio->sodr |= (1 << bit);
	else
		pio->codr |= (1 << bit);
}

int gpio_get_value(int bit)
{
	return (pio->pdsr & (1 << bit)) ? 1 : 0;
}

