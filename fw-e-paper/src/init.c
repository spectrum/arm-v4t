/*
 * arm-v4t at91sam7s
 *
 * init.c
 *
 * Copyright 2016 Angelo Dureghello - Sysam Firmware Solutions
 *
 * This file is part of amtheatre firmware application.
 *
 * arm-v4t is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "reset.h"
#include "clock.h"
#include "time.h"
#include "interrupts.h"

#include <stdio.h>

void system_init(void)
{
	reset_button(1);
	reset_watchdog_disable();

	enable_main_clock();

	init_interrupts();
	init_sys_timer();
}
